

$('.register-card select').styler({
    selectSearch: true
});

$('#user-phone').inputmask("+375 (99) 999-99-99");
$('#user-card-1').inputmask("9  9999  9999  9999");
$('#user-card-2').inputmask("9  9999  9999  9999");
$('#user-sms').inputmask("999 999");

//таймер

var timer2 = "5:01";
var interval = setInterval(function() {
    var timer = timer2.split(':');
    //by parsing integer, I avoid all extra string processing
    var minutes = parseInt(timer[0], 10);
    var seconds = parseInt(timer[1], 10);
    --seconds;
    minutes = (seconds < 0) ? --minutes : minutes;
    if (minutes < 0) clearInterval(interval);
    seconds = (seconds < 0) ? 59 : seconds;
    seconds = (seconds < 10) ? '0' + seconds : seconds;
    //minutes = (minutes < 10) ?  minutes : minutes;
    $('.registr-countdown').html(minutes + ':' + seconds);
    timer2 = minutes + ':' + seconds;
}, 1000);
